import Dependencies._

lazy val commonSettings = inThisBuild(
    Seq(
      scalaVersion := "2.12.8",
      addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
      addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
      addCompilerPlugin(("org.scalamacros" % "paradise"  % "2.1.1") cross CrossVersion.full),
    )
)

lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .settings(
    name := "hue-txt",
    organization := "io.tuliplogic",
    scalaVersion := "2.12.6",
    libraryDependencies ++= Seq(
      zio,
      zioCats,
      zioStreams,
      fastParse,
      sttp,
      sttpZIO,
      sttpCirce,
      circeCore,
      circeGeneric,
      circeParse,
      zioMacrosCore,
      zioMacrosTest,
      scalaTest % Test
    )
  )
