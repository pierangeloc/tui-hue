package io.tuliplogic.hue

import zio._
import zio.console.Console

object Main extends App {

  val tui = Tui[Cmd, State]

  trait LiveConsoleIO extends tui.ConsoleIO with Console {
    override val consoleIO: tui.ConsoleIO.Service[Any] = new tui.ConsoleIO.Service[Any] {
      override def parse(s: String): ZIO[Any, Error, Cmd] = CmdParser.parseInput(s)

      override def render(s: State): ZIO[Any, Nothing, Unit] = console.putStrLn(s.toString)
    }
  }

  trait LiveStateManager extends tui.StateManager with Console {
    val hueClient: HueClient.Service[Any]

    override val stateManager: tui.StateManager.Service[Any] = new tui.StateManager.Service[Any] {
      def transition(cmd: Cmd, state: State): IO[Error, State] =
        cmd match {
          case Cmd.ShowLights()     => hueClient.listLights.map(State.Lights)
          case Cmd.TurnOn(lightId)  => hueClient.turnOn(lightId).as(state)
          case Cmd.TurnOff(lightId) => hueClient.turnOff(lightId).as(state)
          case _                    => UIO.succeed(state)
        }

    }
  }

  val program = tui.TuiModule.>.run(State.Lights(Nil))

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    for {
      sttpBe <- SttpZioBackend.make
      res <- program
              .provide(
                new tui.TuiModule.Live with LiveStateManager with LiveConsoleIO with HueClient.Live with SttpZioBackend
                with Console.Live {
                  override val bridgeIP: String                            = ""
                  override val hueUserId: String                           = ""
                  override val sttpZioBackend: SttpZioBackend.Service[Any] = sttpBe.sttpZioBackend
                }
              )
              .orDie
    } yield res
}

sealed trait State

object State {

  case class Lights(lights: List[LightId]) extends State

}
