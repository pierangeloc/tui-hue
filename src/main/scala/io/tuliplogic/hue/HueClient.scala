package io.tuliplogic.hue

import io.circe.{JsonObject, Error => CirceError}
import io.tuliplogic.hue.Error.APIError
import zio.{IO, UIO, ZIO}

trait HueClient {
  val hueClient: HueClient.Service[Any]
}

object HueClient {

  trait Service[R] {
    def listLights: ZIO[R, APIError, List[LightId]]

    def turnOn(lightId: LightId): ZIO[R, APIError, Unit]

    def turnOff(lightId: LightId): ZIO[R, APIError, Unit]
  }

  trait Live extends HueClient {

    import sttp.client._
    import sttp.client.circe._

    val bridgeIP: String
    val hueUserId: String
    val sttpZioBackend: SttpZioBackend.Service[Any]

    val hueClient: Service[Any] = new Service[Any] {
      override def listLights: ZIO[Any, APIError, List[LightId]] = sttpZioBackend.sttpBackend.flatMap { implicit be =>
        basicRequest
          .get(uri"http://$bridgeIP/api/$hueUserId/lights")
          .response(asJson[Map[String, JsonObject]])
          .send()
          .mapError(e => APIError(e.getMessage, Option(e.getCause)))
          .flatMap(res => processBody(res.body)
            .map(_.keys.toList.map(s => LightId(s.toInt))))
      }

      def processBody[A](body: Either[ResponseError[CirceError], A]): IO[APIError, A] =
        ZIO.fromEither(body).mapError(e => APIError(e.getMessage))

      override def turnOn(lightId: LightId): ZIO[Any, APIError, Unit] = sttpZioBackend.sttpBackend.flatMap { implicit be =>
        basicRequest
          .put(uri"http://$bridgeIP/api/$hueUserId/lights/${lightId.id}/state")
          .body("""{"on": true}""")
          .response(asJson[Map[String, JsonObject]])
          .send()
          .mapError(e => APIError(e.getMessage, Option(e.getCause))).unit
//          .flatMap(res => processBody(res.body).unit)
      }

      override def turnOff(lightId: LightId): ZIO[Any, APIError, Unit] = UIO.effectTotal(println(s"turning off $lightId")) *> sttpZioBackend.sttpBackend.flatMap { implicit be =>
        basicRequest
          .put(uri"http://$bridgeIP/api/$hueUserId/lights/${lightId.id}/state")
          .body("""{"on": false}""")
          .response(ignore)// asJson[Map[String, JsonObject]])
          .send()
          .mapError(e => APIError(e.getMessage, Option(e.getCause))).unit
      }
    }
  }

}