package io.tuliplogic.hue

import sttp.client.SttpBackend
import sttp.client.asynchttpclient.WebSocketHandler
import sttp.client.asynchttpclient.zio.AsyncHttpClientZioBackend
import zio.{Task, UIO, ZIO}


trait SttpZioBackend {
  val sttpZioBackend: SttpZioBackend.Service[Any]
}

object SttpZioBackend {
  type NoStreamingZIOBackend = SttpBackend[Task, Nothing, WebSocketHandler]

  trait Service[R] {
    def sttpBackend: ZIO[R, Nothing, NoStreamingZIOBackend]
  }

  def make: UIO[SttpZioBackend] = AsyncHttpClientZioBackend().map(be => new SttpZioBackend {
    override val sttpZioBackend: Service[Any] = new Service[Any] {
      def sttpBackend: UIO[NoStreamingZIOBackend] = UIO.succeed(be)
    }
  }).orDie
}
