package io.tuliplogic.hue

import io.tuliplogic.hue.Cmd.{ShowLights, TurnOff, TurnOn}
import io.tuliplogic.hue.Error.InvalidInput
import zio.{IO, Task}

case class LightId(id: Int) extends AnyVal

sealed trait Cmd

object Cmd {

  case class ShowLights() extends Cmd

  case class TurnOff(lightId: LightId) extends Cmd

  case class TurnOn(lightId: LightId) extends Cmd

}

object CmdParser {

  import fastparse._, NoWhitespace._

  def number[_: P]: P[Int] = P(CharIn("0-9").rep(1).!.map(_.toInt))

  def showLights[_: P]: P[ShowLights] = P("show lights").map(_ => ShowLights())

  def lightId[_: P]: P[LightId] = number.map(LightId)

  def turnOff[_: P]: P[TurnOff] = P("turn off" ~ " ".rep(1) ~ lightId).map(TurnOff.apply)
  def turnOn[_: P]: P[TurnOn] = P("turn on" ~ " ".rep(1) ~ lightId).map(TurnOn.apply)

  def cmd[_: P]: P[Cmd] = showLights | turnOff | turnOn
  def parseInput(s: String): IO[InvalidInput, Cmd] = Task(parse(s, cmd(_) )).mapError(e => InvalidInput(e.getMessage)).flatMap {
    case Parsed.Success(cmd, _) => IO.succeed(cmd)
    case f: Parsed.Failure => IO.fail(InvalidInput(f.msg))
  }
}
