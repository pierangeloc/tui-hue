package io.tuliplogic.hue

import io.tuliplogic.hue.Error.GenericError
import zio.{ RefM, ZIO }
import zio.console.Console

trait Tui[C, S] {

  trait TuiModule {
    val tuiModule: TuiModule.Service[Any]
  }

  object TuiModule {
    trait Service[R] {
      def run(initial: S): ZIO[R, Error, Nothing]
    }

    trait Live extends TuiModule with Console {
      val consoleIO: ConsoleIO.Service[Any]
      val stateManager: StateManager.Service[Any]

      val tuiModule: Service[Any] = new Service[Any] {
        override def run(initial: S): ZIO[Any, Error, Nothing] = {

          def runLoop(state: RefM[S]): ZIO[Any, Error, Nothing] = {
            def getCmd: ZIO[Any, GenericError, C] =
              for {
                input <- console.getStrLn.mapError(e => GenericError("Error reading from console", Some(e)))
                  cmd <- consoleIO.parse(input).catchAll{
                    e => console.putStrLn("This is not a valid command") *> getCmd
                  }
              } yield cmd

            (for {
              cmd   <- getCmd
              s     <- state.update(stateManager.transition(cmd, _))
              _     <- consoleIO.render(s)
            } yield ()).forever
          }

          RefM.make(initial, 10).flatMap(runLoop)
        }
      }
    }

    object > extends Service[TuiModule] {
      override def run(initial: S): ZIO[TuiModule, Error, Nothing] = ZIO.accessM(_.tuiModule.run(initial))
    }
  }

  trait ConsoleIO {
    val consoleIO: ConsoleIO.Service[Any]
  }

  object ConsoleIO {
    trait Service[R] {
      def parse(s: String): ZIO[R, Error, C]
      def render(s: S): ZIO[R, Nothing, Unit]
    }
  }

  trait StateManager {
    val stateManager: StateManager.Service[Any]
  }

  object StateManager {
    trait Service[R] {
      def transition(cmd: C, state: S): ZIO[R, Error, S]
    }
  }

}

object Tui {
  def apply[C, S]: Tui[C, S] = new Tui[C, S] {}
}
