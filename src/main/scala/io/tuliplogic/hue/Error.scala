package io.tuliplogic.hue

sealed trait Error extends Exception {
  val msg: String
  val cause: Option[Throwable]
  override def getMessage(): String = msg
  override def getCause: Throwable = cause.orNull
}

object Error {
  case class GenericError(msg: String, cause: Option[Throwable] = None) extends Error {
    override def toString: String = s"GenericError. $msg, caused by ${cause.map(_.getStackTrace.mkString("/n"))}"
  }

  case class InvalidInput(msg: String) extends Error {
    val cause: Option[Throwable] = None
  }
  case class APIError(msg: String, cause: Option[Throwable] = None) extends Error
}
